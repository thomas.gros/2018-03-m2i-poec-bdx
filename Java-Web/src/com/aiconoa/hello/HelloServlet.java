package com.aiconoa.hello;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloServlet
 */
@WebServlet("/HelloServlet")
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// status code
		response.setStatus(200);
		
		// headers
		response.setHeader("Content-Type", "text/plain");
		
		// body
//		response.getWriter().append("<br> URL: " + request.getRequestURL());
//		response.getWriter().append("<br> ServletPath:" + request.getServletPath());
//		response.getWriter().append("<br> ContextPath:" + request.getContextPath());
//		response.getWriter().append("<br> PathInfo:" + request.getPathInfo());
		
		response.getWriter()
				.append("GET Served at: ")
				.append(request.getContextPath());
		
		response.getWriter()
				.append("method: ")
				.append(request.getMethod());
		
		response.getWriter()
				.append("url: ")
				.append(request.getRequestURI());
		
		response.getWriter()
				.append("servlet path: ")
				.append(request.getServletPath());
		
		response.getWriter()
				.append("scheme: ")
				.append(request.getScheme());
		
		response.getWriter()
				.append("server name: ")
				.append(request.getServerName());
		
		response.getWriter()
				.append("local port: ")
				.append("" + request.getLocalPort());
		
		response.getWriter()
				.append("path info: ")
				.append(request.getPathInfo());
		
		response.getWriter()
			    .append("user agent: ")
			    .append(request.getHeader("User-Agent"));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("POST Served at: ").append(request.getContextPath());
	}

}
