package com.aiconoa.hello;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TodoListServletWithJSP
 */
@WebServlet("/TodoListServletWithJSP/*")
public class TodoListServletWithJSP extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TodoListServletWithJSP() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		if(pathInfo == null) {
			request.getRequestDispatcher("/todo-list.jsp")
				   .forward(request, response);
			return;
		}
		
		if (! pathInfo.substring(1).matches("\\d+")) {
			response.setStatus(404);
			request.getRequestDispatcher("/404.jsp")
			   	   .forward(request, response);
		}
		
		int id = Integer.parseInt(pathInfo.substring(1));
		TodoListRepository repo = new TodoListRepository();
		ArrayList<Todo> todoList = repo.findTodoListById(id);
		
		if(todoList == null) { // la todo liste existe ?
			response.setStatus(404);
			request.getRequestDispatcher("/404.jsp")
		   	       .forward(request, response);
		}

		request.setAttribute("todolist", todoList);
		
		request
			.getRequestDispatcher("/todo-detail.jsp")
			.forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
