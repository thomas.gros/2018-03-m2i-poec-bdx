<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<a href="http://localhost:8080/JavaWeb/TodoListServletWithJSP">retour vers accueil</a>

<ul>
<c:forEach items="${todolist}" var="item">
    <li>${item.title}</li>
</c:forEach>
</ul>
</body>
</html>