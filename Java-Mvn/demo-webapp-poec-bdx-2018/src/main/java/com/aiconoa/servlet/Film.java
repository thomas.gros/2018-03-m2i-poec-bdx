package com.aiconoa.servlet;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

@Entity
public class Film {
	@Id
	@Column(name="film_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String title;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(
	        name = "film_actor",
	        joinColumns = @JoinColumn(name = "film_id"),
	        inverseJoinColumns = @JoinColumn(name = "actor_id"))
	private Set<Actor> actors;

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}
	
	public Set<Actor> getActors() {
		return actors;
	}
}
