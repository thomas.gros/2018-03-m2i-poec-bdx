package com.mycompany.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class ExampleComTest {
    private static WebDriver driver;
    
    @BeforeAll // avant l'ensemble des tests
    public static void beforeAll() {
        System.setProperty("webdriver.chrome.driver", "/Users/thomasgros/Downloads/chromedriver");
        driver = new ChromeDriver();
    }
    
    @AfterAll // après l'ensemble des tests
    public static void afterAll() {
        driver.quit();
    }
    
    @Test
    void homePageContentIsCorrect() {
        driver.get("http://www.example.com");

        assertEquals("Example Domain", driver.getTitle());

        List<WebElement> allLinks = driver.findElements(By.cssSelector("a"));
        assertEquals(1, allLinks.size());
        
        WebElement l = allLinks.get(0);
        assertEquals("More information...", l.getText());
        assertEquals("http://www.iana.org/domains/example", 
                      l.getAttribute("href"));
    }
    
    @Test
    void navigationIsCorrect() {
        driver.get("http://www.example.com");

        List<WebElement> allLinks = driver.findElements(By.cssSelector("a"));
        
        WebElement l = allLinks.get(0);
        l.click();
        assertEquals("IANA — IANA-managed Reserved Domains", driver.getTitle());
    }

}
