package com.mycompany.app;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumDemo {

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", 
                           "/Users/thomasgros/Downloads/chromedriver");
        WebDriver driver = new ChromeDriver(); // Chrome
        // WebDriver driver = new FirefoxDriver(); // Firefox
        driver.get("http://www.example.com");
        
        System.out.println(driver.getTitle());
        List<WebElement> links = driver.findElements(By.tagName("a"));
        
        for (WebElement webElement : links) {
            System.out.println(webElement.getText());
            System.out.println(webElement.getAttribute("href"));
        }
       
        List<WebElement> allLinks = driver.findElements(By.cssSelector("a"));
        WebElement l = allLinks.get(0);
        l.click();
        
       driver.quit();
    }

}
