package com.mycompany.app;

/**
 * Hello world!
 *
 */
public final class App {
    /**
     * Interdiction d'instancier la classe Main.
     */
    private App() { }

    /**
     * Point d'entrée du programme.
     * 
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
