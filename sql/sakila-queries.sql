# tous les clients
SELECT * FROM customer;

# tous les films
SELECT * FROM film;

# tous les employés
SELECT * FROM staff;

# tous les magasins
SELECT * FROM store;

# le nom et le prénom de tous les employés
SELECT first_name, last_name FROM staff;

# le titre et la durée de tous les films
SELECT title, length FROM film;

# numéro client et date de location pour toutes les locations
SELECT customer_id, rental_date FROM rental;

# le titre des films qui durent plus que 50 minutes
SELECT title FROM film WHERE length > 50;

# toutes les informations sur les locations du client 71
SELECT * FROM rental WHERE customer_id = 71;


# toutes les informations sur les locations du client 71 
# ultérieures au 27 Juillet 2005
SELECT * FROM rental WHERE customer_id = 71 AND rental_date > '2005-07-27';

# toutes les informations sur les locations du client 71 
# ultérieures au 27 Juillet 2005 en ordre chronologique descendant
SELECT * 
FROM rental 
WHERE customer_id = 71 AND rental_date > '2005-07-27'
ORDER BY rental_date DESC;

# les 3 locations les plus récentes du client 71
SELECT *
FROM rental
WHERE customer_id = 71 
ORDER BY rental_date DESC
LIMIT 3;





# JOINTURES INTERNES

SELECT city.*, country.*
FROM city INNER JOIN country
	ON city.country_id = country.country_id;


SELECT city.city, country.country
FROM city INNER JOIN country
	ON city.country_id = country.country_id;
    
    
    
SELECT customer.*, address.*
FROM customer INNER JOIN address
	ON customer.address_id = address.address_id;
    
    
SELECT staff.*, store.*
FROM staff INNER JOIN store
ON staff.store_id = store.store_id;

    
# jointures internes multiples    
SELECT customer.*, address.*, city.*
FROM customer 
	INNER JOIN address
		ON customer.address_id = address.address_id
	INNER JOIN city
		ON address.city_id = city.city_id;
        
SELECT customer.last_name, address.address, city.city
FROM customer 
	INNER JOIN address
		ON customer.address_id = address.address_id
	INNER JOIN city
		ON address.city_id = city.city_id;
        
# Quels sont les acteurs ayant joué dans chaque film ?
# film.title, actor.first_name, actor.last_name
        
SELECT film.title, actor.first_name, actor.last_name
FROM film 
	INNER JOIN film_actor
		ON film.film_id = film_actor.film_id
	INNER JOIN actor
		ON film_actor.actor_id = actor.actor_id;
    
# Les locations qui ont été effectuées par chaque client
# (utiliser uniquement customer et rental)
SELECT customer.*, rental.*
FROM customer
	INNER JOIN rental
		ON customer.customer_id = rental.customer_id;


# En vous basant sur la requête précédente
# lister les clients et les titres des films qu'ils ont loué
# customer.first_name, customer.last_name, film.title
SELECT customer.first_name, customer.last_name, film.title
FROM customer
	INNER JOIN rental
		ON customer.customer_id = rental.customer_id
	INNER JOIN inventory
		ON rental.inventory_id = inventory.inventory_id
	INNER JOIN film
		ON film.film_id = inventory.film_id
        
ORDER BY customer.last_name ASC, customer.first_name ASC;
        

# En vous basant sur la requête précédente
# lister les titres de films qu'a loué Vickie Brewer
# customer.first_name, customer.last_name, film.title

SELECT customer.first_name, customer.last_name, film.title
FROM customer
	INNER JOIN rental
		ON customer.customer_id = rental.customer_id
	INNER JOIN inventory
		ON rental.inventory_id = inventory.inventory_id
	INNER JOIN film
		ON film.film_id = inventory.film_id
	  AND customer.first_name = 'VICKIE' 
	  AND customer.last_name = 'BREWER';


# Trouver le nom et le prenom des acteurs qui n'ont pas joué dans le film
# d'id 1
SELECT actor.first_name, actor.last_name
FROM film_actor RIGHT OUTER JOIN actor
ON film_actor.actor_id = actor.actor_id
AND film_actor.film_id = 1
WHERE film_actor.actor_id IS NULL;



# Functions and Operators
# numeriques
SELECT film_id, title, length * 60 AS length_in_s, RAND()  as a_random_number
FROM film
WHERE length * 60 >  80 * 60;

# chaines de caractères
SELECT CHAR_LENGTH(first_name) + CHAR_LENGTH(last_name) - CHAR_LENGTH(email)
FROM customer;

SELECT CONCAT(first_name, '.', last_name, '@sakilacustomer.com')
FROM customer;

SELECT customer_id, email
FROM customer
WHERE CHAR_LENGTH(email) > 35;

SELECT customer_id, email
FROM customer
ORDER BY CHAR_LENGTH(email) DESC
LIMIT 3;

# Control Flow

SELECT film_id, title, length, 
								   CASE  
										WHEN length > 120 THEN 'long'
										WHEN length > 90 THEN 'moyen'
                                        WHEN length > 60 THEN 'court'
                                        ELSE 'très court'
									END
									AS longueur
FROM film
ORDER BY length;

SELECT DISTINCT first_name # tous les prénoms distincts
FROM customer;

# Aggregations
# sur l'ensemble des lignes
SELECT COUNT(*) as nb_customers # compter les lignes
FROM customer;

SELECT COUNT(DISTINCT first_name) as total_first_name # compter les prénoms différents
FROM customer;

SELECT AVG(length), SUM(length), COUNT(*), MAX(length), MIN(length)
FROM film;

# sur des groupes de lignes
SELECT AVG(length), SUM(length), COUNT(*), MAX(length), MIN(length), rating
FROM film
GROUP BY rating;

SELECT COUNT(*) as nb_films, rating
FROM film
GROUP BY rating
ORDER BY nb_films DESC
LIMIT 1;


SELECT SUM(payment.amount), rental.customer_id 
FROM rental 
	INNER JOIN payment ON rental.rental_id = payment.rental_id
GROUP BY rental.customer_id
ORDER BY SUM(payment.amount) DESC
LIMIT 3;

SELECT AVG(length), rental_duration
FROM film
GROUP BY rental_duration
HAVING AVG(length) > 113;

# SubQueries
# les films dont la longueur est supérieure à la moyenne
SELECT * 
FROM film
WHERE length > (SELECT AVG(length) FROM film);

# les emails de tous les clients qui ont des films non retournés
SELECT email FROM customer
WHERE customer_id IN (SELECT customer_id FROM rental WHERE return_date IS NULL);

# clients qui n'ont pas rendu le film alors que la durée de location est écoulée
SELECT CONCAT(customer.first_name, ' ', customer.last_name), customer.email, film.title
FROM customer 
	INNER JOIN
		(SELECT customer_id, inventory_id, rental_date
         FROM rental WHERE return_date IS NULL) AS C #ici la subquery n'est pas nécessaire
        ON customer.customer_id = C.customer_id
	INNER JOIN inventory
		on inventory.inventory_id = C.inventory_id
	INNER JOIN film
		on film.film_id = inventory.film_id
WHERE DATEDIFF(NOW(), C.rental_date) > film.rental_duration;














































