package com.aiconoa.java.test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ATest {

	@ParameterizedTest
	@CsvSource({ "black, #000000", "white, #FFFFFF", "red, #FF0000" })
	public void shouldGenerateHexValueForColor(
			String colorName,
			String expectedHexValue) {
		
		A a = new A();
		String actualResult = a.getHexColor(colorName);
		
		assertEquals(expectedHexValue, actualResult, "should be #000000");	
	}
	
//	@Test
//	public void shouldGenerate000000WhenBlackColor() {
//		String name = "black";
//		
//		A a = new A();
//		String actualResult = a.getHexColor(name);
//		
//		String expectedResult = "#000000";
//		
//		assertEquals(expectedResult, actualResult, "should be #000000");
//	}
	
	@Test
	public void shouldThrowRuntimeExceptionIfUnsupportedColor() {
	
		// Pre Java 8: classe anonyme instanciée à la volée
//		Executable e = new Executable() {
//			
//			@Override
//			public void execute() throws Throwable {
//				String name = "abcdef";
//				A a = new A();
//				a.getHexColor(name);
//				
//			}
//		};
		
		// Java 8+: function lambda
		Executable e = () -> {
			String name = "abcdef";
			A a = new A();
			a.getHexColor(name);
		};
		
		Throwable t = assertThrows(RuntimeException.class, e);
		assertEquals(t.getMessage(), "Unsupported color");
	}
	
	@Test
	public void shouldThrowNullPointerExceptionIfColorNameIsNull() {
		Throwable t = assertThrows(
				NullPointerException.class, 
				() -> {
					A a = new A();
					a.getHexColor(null);
				});
		
		assertEquals(t.getMessage(), "Name must not be null");
	}
	
}
