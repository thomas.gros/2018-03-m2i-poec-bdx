package com.aiconoa.java.oop;

public class Circle implements Shape {

	double r;
	
	public double calcArea() {
		return Math.PI * Math.pow(r, 2);
	}
	
}
