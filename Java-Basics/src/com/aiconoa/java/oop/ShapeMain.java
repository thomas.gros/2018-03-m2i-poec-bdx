package com.aiconoa.java.oop;

public class ShapeMain {

	public static void main(String[] args) {
		// Créer une classe Rectangle qui possède une méthode 
		// public int calcArea(int width, int height)

		Rectangle r1 = new Rectangle(10, 50);
		
		System.out.println(r1.calcArea());
		
		Circle c = new Circle();
		c.r = 4;
		System.out.println(c.calcArea());
		
		Shape[] tab = { r1, c };
		
		for (int i = 0; i < tab.length; i++) {
			Shape o = tab[i];
			System.out.println(o.calcArea());
		}

		
	}

}
