package com.aiconoa.java.oop;

public interface Shape {
	
	// les méthodes d'une interface sont public abstract
	double calcArea();

	// depuis Java 8: default methods
//	default double calcArea() {
//		return 0;
//	}

}
