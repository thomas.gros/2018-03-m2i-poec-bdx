package com.aiconoa.java.oop;

public class SingletonMain {

	public static void main(String[] args) {
		Singleton s = Singleton.INSTANCE;
		Singleton s2 = Singleton.INSTANCE;

		System.out.println(s == s2); // true

	}

}
