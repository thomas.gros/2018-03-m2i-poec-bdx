package com.aiconoa.java.oop.collection;

import java.util.ArrayList;

import com.aiconoa.java.oop.Circle;
import com.aiconoa.java.oop.Rectangle;
import com.aiconoa.java.oop.Shape;

public class ListMain {

	public static void main(String[] args) {
		ArrayList<Shape> myList = new ArrayList<Shape>();
		
		// LinkedList<Shape> myList = new LinkedList<Shape>();
		
		myList.add(new Rectangle(10, 10));
		myList.add(new Circle());
		
		Circle c1 = new Circle();
		myList.add(c1);
		myList.add(c1);
		
		System.out.println(myList.size());
		
		for (int i = 0; i < myList.size(); i++) {
			System.out.println(myList.get(i));
		}
		
		for (Shape shape : myList) {
			System.out.println(shape);
		}

	}

}
