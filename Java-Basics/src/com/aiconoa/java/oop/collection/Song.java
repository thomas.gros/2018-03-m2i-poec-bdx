package com.aiconoa.java.oop.collection;

import com.aiconoa.java.oop.Personne;

public class Song {

	private String title;
	private Personne author;
	private String genre;
	private int duration;
	
	public Song(String title, Personne author, String genre, int duration) {
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.duration = duration;
	}

	public String getTitle() {
		return title;
	}

	public Personne getAuthor() {
		return author;
	}

	public String getGenre() {
		return genre;
	}

	public int getDuration() {
		return duration;
	}

	@Override
	public String toString() {
		return "["+
				title + " " +
				author + " " +
				genre + " " +
				duration +
				"]";
	}
	
}
