package com.aiconoa.java.oop;

public class Singleton {
	public static final Singleton INSTANCE = new Singleton();
	
	private Singleton() {}
	
}
