package com.aiconoa.java.oop;

public class Notes {

	public static void main(String[] args) {
		int[] notes = {18, 20, 2, 14, 7, 10};
		
		// 1) calculer la moyenne des notes
		int sum = 0;
		for (int i = 0; i < notes.length; i++) {
			sum += notes[i];
		}
		System.out.println("sum: " + sum);
		// double moyenne = (double) sum / notes.length; // cast possible
		double moyenne = 1.0 * sum / notes.length;
		System.out.println("avg: " + moyenne); // avg, mean
		
		// 2) trouver la plus petite note
		// int min = Integer.MAX_VALUE;
		int min = notes[0];
		for (int i = 1; i < notes.length; i++) {
			if(notes[i] < min) {
				min = notes[i];
			}
		}
		System.out.println("min: " + min);
		
		// 3) trouver la plus grande note
		int max = notes[0];
		// int max = Integer.MIN_VALUE;
		for (int i = 1; i < notes.length; i++) {
			if(notes[i] > max) {
				max = notes[i];
			}
		}
		
		System.out.println("max: " + max);
		
		// 4) calculer la variance
		// 5) calculer l'ecart type (stddev)
		

	}

}
