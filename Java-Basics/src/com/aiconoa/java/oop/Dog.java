package com.aiconoa.java.oop;

public class Dog extends Animal {
	
	public void bark() {
		System.out.println("Woof");
	}
	
	public void debug() {
		System.out.println(name);
	}

	@Override
	public void makeSound() {
		// super.makeSound();
		bark();
	}
}