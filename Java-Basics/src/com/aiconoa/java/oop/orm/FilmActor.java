package com.aiconoa.java.oop.orm;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="film_actor")
public class FilmActor implements Serializable {

	@Id
	// attention Hibernante est EAGER pour des relation to-one
	// http://docs.jboss.org/hibernate/orm/5.2/userguide/html_single/Hibernate_User_Guide.html#fetching
	// https://www.thoughts-on-java.org/best-practices-many-one-one-many-associations-mappings/
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="film_id",
			    foreignKey=@ForeignKey(name ="fk_film_actor_film"))
    private Film film;

    @Id
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="actor_id",
    			foreignKey=@ForeignKey(name ="fk_film_actor_actor"))
    private Actor actor;
    
    public Film getFilm() {
		return film;
	}
    
    public Actor getActor() {
		return actor;
	}
}
