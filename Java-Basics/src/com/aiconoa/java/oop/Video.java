package com.aiconoa.java.oop;

public class Video {

	private Integer id;
	private String title;
	
	private User author; // using OOP instead of mere author_id int field
	
	// private List<Comment> comments; // TODO
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public User getAuthor() {
		return author;
	}
	
	public void setAuthor(User author) {
		this.author = author;
	}
}
