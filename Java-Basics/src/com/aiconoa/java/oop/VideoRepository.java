package com.aiconoa.java.oop;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class VideoRepository {
	private Video mapResultSetToVideo(ResultSet rsVideo) throws SQLException  {
		// maps the video
		Video v = new Video();
		v.setId(rsVideo.getInt("video.id")); // ou rs.getInt(1)
		v.setTitle(rsVideo.getString("video.title"));

		// maps the user
		User u = new User();
		u.setId(rsVideo.getInt("user.id"));
		u.setUsername(rsVideo.getString("user.username"));
		u.setPassword(rsVideo.getString("user.password"));
		u.setEmail(rsVideo.getString("user.email"));
		
		// maps the relation video authored by user 
		v.setAuthor(u);
		
		return v;
	}


	/**
	 * Fetch a video by its id.
	 * @param id the video id
	 * @return the video corresponding to the id or null if no user was found
	 * @throws RuntimeException if a problem occurs.
	 */
	public Video findById(int id) {
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/poec?serverTimezone=UTC", "root", "rootroot")) {

			// fetch the video
			PreparedStatement pstmtVideo = 
					conn.prepareStatement(
"SELECT video.*, user.* FROM video INNER JOIN user ON video.author_id = user.id WHERE id = ?");
			pstmtVideo.setInt(1, id);
			
			ResultSet rsVideo = pstmtVideo.executeQuery();
			if(! rsVideo.next()) { 
				return null; 
			}
			
			// build the video instance from both resultSets
			return  mapResultSetToVideo(rsVideo);
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
