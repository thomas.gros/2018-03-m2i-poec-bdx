package com.aiconoa.java.oop;

import java.math.BigDecimal;

public class TransactionMain {

	public static void main(String[] args) {

		// Une transaction a deux variables d'instance:
		//	- type: String ("debit" ou "credit")
		//  - amount: double (toujours positif)
		
		// Créer 4 transactions:
		// débit: 10, crédit: 20, débit: 30, crédit: 15
		Transaction t1 = new Transaction("debit", new BigDecimal("10"));

		Transaction t2 = new Transaction("credit", new BigDecimal("20"));
		
		Transaction t3 = new Transaction("debit", new BigDecimal("30"));
		
		Transaction t4 = new Transaction("credit", new BigDecimal("15"));
		
		Transaction[] transactions = new Transaction[4];
		transactions[0] = t1;
		transactions[1] = t2;
		transactions[2] = t3;
		transactions[3] = t4;
		
		// Transaction[] transactions = {t1, t2, t3, t4}
		
		// exercice calculer le total des transactions
		BigDecimal total = BigDecimal.ZERO; // new BigDecimal("0");
		for (int i = 0; i < transactions.length; i++) {
			if(transactions[i].getType().equals("debit")) {
				total = total.subtract(transactions[i].getAmount());
			} else if (transactions[i].getType().equals("credit")) {
				total = total.add(transactions[i].getAmount());
			} else {
				System.out.println("type de transaction inconnu - gérer l'erreur plus proprement");
			}
		}
		
		System.out.println("Total des transactions: " + total);
		
	}

}
