package com.aiconoa.java.basics;

public class TableMultiplicationMain {

	public static void main(String[] args) {
		TableMultiplication table = new TableMultiplication();

		table.print(2, 50);
		// 2*1 = ..
		// 2*2 = ..
		// ...
		// 2*20 = ..

	}

}
