package com.aiconoa.java.basics;

public class PrimitivesTypes {

	public static void main(String[] args) {
		// primitifs
		
		// integer
		int monInteger;
		monInteger = 12;
		
		short monEntierShort;
		monEntierShort = 32;
		
		byte monEntierByte;
		monEntierByte = 55;
		
		long monEntierLong;
		monEntierLong = 180L;
		
		// floating point
		float monFloat = 1.5F;
		double monDouble = 3.5;
		
		// http://floating-point-gui.de
		// https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html
		// System.out.println(0.1 + 0.2); // IEEE 754 !!!
		
		boolean monBooleen = true; // ou false
		
		char monCharacter = 'a';
		
		
		
	}

}
