package com.aiconoa.java.basics;

import java.util.Scanner;

public class GuessANumber {
	public static void main(String[] args) {

		// V1
		// écrire un programme qui demande
		// à l'utilisateur de deviner un nombre
		// entre 1 et 10.
		// Tant que l'utilisateur n'a pas deviné le bon
		// nombre, écrire "perdu" puis
		// redemander à deviner.

		// quand l'utilisateur a deviné le bon nombre,
		// écrire "gagné" et quitter

		// V1.1
		// écrire "gagné en X coups"

		int nombreADeviner = 8;
		Scanner sc = new Scanner(System.in);
		
		int compteur = 0;
		for (;;) {
			
			int choixUtilisateur = sc.nextInt();
			compteur++;
			
			if (nombreADeviner == choixUtilisateur) {
				System.out.println("Gagné en " + compteur + " coups");
				break;
			} else {
				System.out.println("Perdu");
			}
		}

	}
}
