package com.aiconoa.java.basics;

public class Mention {

	public static void main(String[] args) {
		// si note < 10 : 'peut mieux faire'
		// si 10 <= note < 12 : 'passable'
		// si 12 <= note < 14 : 'assez bien'
		// si 14 <= note < 16 : 'bien'
		// si 16 <= note : 'très bien'
		int note = 17;

		if (note < 10) {
			System.out.println("peut mieux faire");
		} else if (note < 12) {
			System.out.println("passable");
		} else if (note < 14) {
			System.out.println("assez bien");
		} else if (note < 16) {
			System.out.println("bien");
		} else {
			System.out.println("très bien");
		}

	}

}