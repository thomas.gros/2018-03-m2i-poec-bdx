package com.aiconoa.java.basics;

public class UtilNote {

	public double calcMoyenne(int[] tabNotes) { // tabNotes = notes
		int sum = 0;
		for (int i = 0; i < tabNotes.length; i++) {
			sum += tabNotes[i];
		}
		double moyenne = 1.0 * sum / tabNotes.length;
		return moyenne;
	}

	public int findMin(int[] tabNotes) {
		int min = tabNotes[0];
		for (int i = 1; i < tabNotes.length; i++) {
			if (tabNotes[i] < min) {
				min = tabNotes[i];
			}
		}
		
		return min;
	}

	public int findMax(int[] notes) {
		int max = notes[0];
		for (int i = 1; i < notes.length; i++) {
			if(notes[i] > max) {
				max = notes[i];
			}
		}
		
		return max;
	}

}
