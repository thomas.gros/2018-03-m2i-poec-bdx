package com.aiconoa.java.basics;

public class Operator {

	public static void main(String[] args) {
		System.out.println(1 + 1);
		
		System.out.println(40 / 3);
		System.out.println(1 / 3);
		System.out.println(1.0 * 1 / 3);
		
		System.out.println(1 + ((2 * 3) % 4));

		// modulo
		// reste de la division euclidienne
		
		// a % b    => a = q * b + r
		// 4 % 2	=> 4 = q * 2 + r => r = 0
		// 10 % 3   => 10 = q * 3 + r => r = 1
		
		// a % b  == 0 alors a est divisible par b
		// a % 2 == 0
		System.out.println(4 % 2);
		
		int i = 42;
		int a = 0;
		a = i++; // attention post incrémentation
		System.out.println(a);
		System.out.println(i);
		
		i += 10;  // i = i + 10;   // a -= v <=> a = a - v

		// operateurs booleans
		System.out.println(5 == 3);
		System.out.println(5 != 3);
		
		System.out.println(5 >= 3);
		System.out.println(5 > 3);
		System.out.println(5 <= 3);
		System.out.println(5 < 3);	
	}

}
